var pos = 0;
var prox = 1;
var autop = true;

$(document).ready(function() {
    setTimeout(function() {
        $(".loader").addClass("complete");
    }, 2000);
    $(".carousel").carousel({
        fullWidth: true,
        onCycleTo: function(ele) {
            console.log($(ele).index()); // the slide's index
            pos = $(ele).index();

            if (pos == prox) {
                autop = false;
            }

            prox = $(ele).index() + 1;
            if (prox == 13) {
                prox = 0;
            }
        }
    });

    $(".scrollspy").scrollSpy();

    $(".modal").modal();

    if (autop == true) {
        setTimeout(autoplay, 4500);
    }

    function autoplay() {
        $(".carousel").carousel("next");
    }

    $(".galeria").slick({
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        adaptiveHeight: true,
        lazyLoad: "ondemand",
        autoplay: true,
        autoplaySpeed: 5000
    });

    AOS.init({
        // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
        offset: 100, // offset (in px) from the original trigger point
        delay: 250, // values from 0 to 3000, with step 50ms
        duration: 400, // values from 0 to 3000, with step 50ms
        easing: "ease", // default easing for AOS animations
        once: true, // whether animation should happen only once - while scrolling down
        mirror: false // whether elements should animate out while scrolling past them
    });

    $(".slider-top").slick({
        autoplay: true,
        infinite: true,
        speed: 1000,
        cssEase: "ease",
        lazyLoad: "ondemand",
        autoplay: true,
        autoplaySpeed: 3500,
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev top">Previous</button>',
        nextArrow: '<button type="button" class="slick-next top">Next</button>',
        dots: false
    });
});

function triggerModal() {
    $("#modal1").modal("open");
}